
import random

minimo = 99
maximo = 0
print ("Se generan 10 numeros aleatorios entre [0-99]: ")

print ("Los numeros son: ")

for i in range (0,10,1):
    numero = random.randrange(0,99, 1)
    if(numero < minimo ):
        minimo = numero
    if(numero> maximo ):
        maximo = numero
    if(i==0):
        print ( "[ " + str (numero)  + ", ",end="")
    if(i<9 and i >0):
        print ( str (numero)  + ", ",end="")
    if(i==9):
        print ( str (numero)+ " ]")

print ("El mayor numero es: " + str (maximo))
print ("El menor numero es: " + str (minimo))