from Clase_equipo       import Equipo
from random             import randrange

class Partido:
    def __init__(self,fecha,local,visitante,cancha,clima):
        self.goals_visitante        =-1
        self.goals_local            =-1
        self.yellow_visitante       =-1
        self.yellow_local           =-1
        self.red_visitante          =-1
        self.red_local              =-1
        self.possession_visitante   =-1 
        self.possession_local       =-1 
        self.attempts_visitante     =-1
        self.attempts_local         =-1
        self.ontarget_local         =-1
        self.ontarget_visitante     =-1
        self.corners_local          =-1
        self.corners_visitante      =-1
        self.offside_local          =-1
        self.offside_visitante      =-1 
        self.passes_local           =-1
        self.passes_visitante       =-1 
        self.completed_local        =-1
        self.completed_visitante    =-1
        self.fouls_local            =-1
        self.fouls_visitante        =-1       

        self.weather_list=["Soleado","Nublado","Lluvia","Neblina"]
        self.fecha = fecha

        if type(local) == Equipo:
            self.local = local
        else:
            print("Error: ",team,"invalid type")
        
        if type(visitante) == Equipo:
            self.visitante = visitante
        else:
            print("Error Equipo: ",team,"invalid type")
        
        if isinstance (cancha,str):
            self.cancha = cancha
        else:
           print ( "Error Nombre Cancha: ",cancha,"invalid type")
        
        if isinstance (clima,str):
            if len(clima)!=0:
                if clima in self.weather_list:
                    self.clima = clima
                else:
                    print ("Error Clima: ",clima, "invalid")
            else:
                index_weather = randrange(len(self.weather_list))
                self.clima = self.weather_list[index_weather]
        else:
           print ( "Error Clima: ",clima,"invalid type")             
    
    def play_game(self,goles_visitante=-1,goles_local=-1,amarilla_visitante=-1,amarilla_local=-1,roja_visitante=-1,roja_local=-1):
        if goles_visitante == -1:
            self.goals_visitante    = randrange(10)
        else:
            self.goals_visitante    = goles_visitante
        
        if goles_local == -1:
            self.goals_local        = randrange(10)
        else:
            self.goals_local        = goles_local            

        if amarilla_visitante == -1:
            self.yellow_visitante = randrange(4)
        else:
            self.yellow_visitante = amarilla_visitante    

        if amarilla_local == -1:
            self.yellow_local     = randrange(4)
        else:
            self.yellow_local     = amarilla_local 

        if roja_visitante == -1:
            self.red_visitante     = randrange(3)
        else:
            self.red_visitante      = roja_visitante 

        if roja_local == -1:
            self.red_local         = randrange(3)
        else:
            self.red_local        = roja_local
        
        
        self.possession_local       = randrange(101)
        self.possession_visitante   = 100 - self.possession_local
        self.attempts_visitante     = randrange(1000)
        self.attempts_local         = randrange(1000)
        self.ontarget_local         = self.attempts_local - randrange(1000)
        
        while self.ontarget_local < 0:
            self.ontarget_local         = self.attempts_local - randrange(1000)

        self.ontarget_visitante     = self.attempts_visitante - randrange(1000)
        
        while self.ontarget_visitante < 0:
            self.ontarget_visitante     = self.attempts_visitante - randrange(1000)

        self.corners_local          = randrange(11)
        self.corners_visitante      = randrange(11)
        self.offside_local          = randrange(11)
        self.offside_visitante      = randrange(11)
        self.passes_local           = randrange(1000)
        self.passes_visitante       = randrange(1000)
        self.completed_local        = self.passes_local - randrange(1000)
        while self.completed_local < 0:
            self.completed_local        = self.passes_local - randrange(1000)

        self.completed_visitante    = self.passes_visitante - randrange(1000)
        while self.completed_visitante < 0:
            self.completed_visitante    = self.passes_visitante - randrange(1000)

        self.fouls_local            = self.red_local      + self.yellow_local + randrange(11)
        self.fouls_visitante        = self.red_visitante  + self.yellow_visitante  + randrange(11)

    def result_winner(self):
        if self.goals_visitante  == -1 or self.goals_local == -1:
            print ("Error: No se jugo el partido")
        else:
            if self.goals_visitante > self.goals_local:
                return self.visitante
            elif self.goals_local  > self.goals_visitante:
                return self.local
            elif self.goals_visitante  == self.goals_local:
                return (-1)         

    def result_loser(self):
        if self.goals_visitante  == -1 or self.goals_local == -1:
            print ("Error: No se jugo el partido")
        else:        
            if  self.goals_visitante  < self.goals_local:
                return self.visitante
            elif self.goals_local < self.goals_visitante :
                return self.local
            elif self.goals_visitante  == self.goals_local:
                return (-1) 

    def statistics  (self):
        print ("\t\t"  + self.local.name()         + "\t \t" + self.visitante.name())
        print ("\t\t"+ str (self.goals_local)      +"\t   GOALS  \t"  + str (self.goals_visitante))
        print ("\t\t"+ str (self.possession_local) +"\tPOSSESSION\t"  + str (self.possession_visitante))        
        print ("\t\t"+ str (self.attempts_local)   +"\t ATTEMPTS \t"  + str (self.attempts_visitante))        
        print ("\t\t"+ str (self.ontarget_local)   +"\tON  TARGET\t"  + str (self.ontarget_visitante))        
        print ("\t\t"+ str (self.corners_local)    +"\t  CORNERS \t"  + str (self.corners_visitante))
        print ("\t\t"+ str (self.offside_local)    +"\t  OFFSIDE \t"  + str (self.offside_visitante))
        print ("\t\t"+ str (self.passes_local)     +"\t  PASSES  \t"  + str (self.passes_visitante ))
        print ("\t\t"+ str (self.completed_local)  +"\tCOMPLETED \t"  + str (self.completed_visitante))
        print ("\t\t"+ str (self.fouls_local)      +"\t  FOULS \t"    + str (self.fouls_visitante))
        print ("\t\t"+ str (self.yellow_local)     +"\t  YELLOW  \t"  + str (self.yellow_visitante))
        print ("\t\t"+ str (self.red_local)        +"\t    RED   \t"  + str (self.red_visitante))        

    def __str__(self):
        return '{0}: ({1}) vs {2}: ({3})'.format(self.local.name(),self.goals_local,self.visitante.name(),self.goals_visitante)

                                        