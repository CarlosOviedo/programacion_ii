from Clase_equipo       import Equipo
from Clase_partido    import Partido
from random             import randrange

class Campeonato:
    def __init__(self,nombre,n_equipos):
        self.team_list=[]
        self.games_list=[]
        if isinstance (nombre,str):
            self.nombre = nombre
        else:
           print ( "Error Nombre: ",nombre,"invalid type") 
        
        if isinstance (n_equipos,int):
            self.n_equipos=n_equipos
        else:
           print ( "Error N° Equipos: ",n_equipos,"invalid type") 
        #Numero de Partidos
        self.n_partidos=((n_equipos*(n_equipos-1))/2)

    def __str__(self):
        return '{0}: {1} | {2} '.format(self.nombre,self.n_equipos,self.n_partidos)
 
    def add_team (self,team):
        if type(team) == Equipo:
            self.team_list.append(team) 
        else:
            print("Error Equipo: ",team,"invalid type")
    
    def play_campeonato(self):
        if (len(self.team_list)== 0):
            print ("Error: No hay equipos cargados")
        else:
            for i in range (0,self.n_equipos):
                for j in range (0,self.n_equipos):
                    if i!=j:
                       aux_var = Partido("none",self.team_list[i],self.team_list[j],"","") 
                       self.games_list.append(aux_var)
                       aux_var.play_game()
                       if (self.team_list[i] == aux_var.result_winner()):
                           self.team_list[i].played_game(1 ,1 ,0 ,0 ,0 ,0 ,3)
                           self.team_list[j].played_game(1 ,0 ,0 ,1 ,0 ,0 ,0)
                       elif (self.team_list[i] == aux_var.result_loser()):
                           self.team_list[i].played_game(1 ,0 ,0 ,1 ,0 ,0 ,0)
                           self.team_list[j].played_game(1 ,1 ,0 ,0 ,0 ,0 ,3)                             
                       else: 
                            self.team_list[i].played_game(1 ,0 ,1 ,0 ,0 ,0 ,1)
                            self.team_list[j].played_game(1 ,0 ,1 ,0 ,0 ,0 ,1)

        for item in self.games_list:
            print(item)

            


    def fixture(self):
        #PJ: Partidos Jugados   
        #PG: Partidos Ganados
        #PE: Partidos Empatados
        #PP: Partidos Perdidos
        #GA: Goles a Favor
        #GC: Goles en Contra
        #Pt: Puntos
        self.team_list.sort(reverse=True)
        print("Equipo       :\t\t PJ\t PG\t PE\t PP\t GA\t GC\t Pt")
        for equipo in self.team_list:
            print(equipo) 
