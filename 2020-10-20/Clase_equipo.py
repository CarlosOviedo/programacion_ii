class Equipo:

    def __init__ (self,nombre,cancha):
        self.PJ=0
        self.PG=0
        self.PE=0
        self.PP=0
        self.GA=0
        self.GC=0
        self.PT=0
        if isinstance (nombre,str):
            self.nombre = nombre
        else:
           print ( "Error Nombre Equipo: ",nombre,"invalid type") 
        
        if isinstance (cancha,str):
            if len(cancha)==0:
                self.cancha = "Cancha de "+self.nombre
            else:
                 self.cancha=cancha   
        else:
           print ( "Error Nombre Cancha: ",nombre,"invalid type")
    
    def __str__(self):
        return '{0}:\t\t {1} \t| {2} \t| {3} \t| {4} \t| {5} \t| {6} \t| {7}'.format(self.nombre,self.PJ,self.PG,self.PE,self.PP,self.GA,self.GC,self.PT)

    def played_game(self,partidos_jugados,partidos_ganados,partidos_empatados,partidos_perdidos,goles_afavor,goles_contra,puntos):
        
        if isinstance (partidos_jugados,int):
            self.PJ=self.PJ+partidos_jugados
        else:
           print ( "Error PJ: ",partidos_jugados,"invalid type")

        if isinstance (partidos_ganados,int):
            self.PG=self.PG+partidos_ganados
        else:
           print ( "Error PG: ",partidos_ganados,"invalid type")
       
        if isinstance (partidos_empatados,int):
            self.PE=self.PE+partidos_empatados
        else:
           print ( "Error PE: ",partidos_empatados,"invalid type")

        if isinstance (partidos_perdidos,int):
            self.PP=self.PP+partidos_perdidos
        else:
           print ( "Error PP: ",partidos_perdidos,"invalid type")

        if isinstance (goles_afavor,int):
            self.GA=self.GA+goles_afavor
        else:
           print ( "Error GA: ",goles_afavor,"invalid type")

        if isinstance (goles_contra,int):
            self.GC=self.GC+goles_contra
        else:
           print ( "Error GC: ",goles_contra,"invalid type")

        if isinstance (puntos,int):
            self.PT=self.PT+puntos
        else:
           print ( "Error PT: ",puntos,"invalid type")

   
    def name (self):
        return(self.nombre)
    def field(self):
        return self.cancha
   
    def edit_nombre(self,nombre):
        if isinstance (nombre,str):
            self.nombre = nombre
        else:
           print ( "Error Edit Nombre: ",nombre,"invalid type") 



    def __lt__(self, equipo): #
        if type(equipo) == Equipo:
            if self.PT < equipo.PT:
                return True
            else:
                return False
        else:
            print("Error Equipo: ",equipo,"invalid type")

    def __gt__(self, otro): #
        if type(equipo) == Equipo:
            if self.PT > equipo.PT:
                return True
            else:
                return False
        else:
            print("Error Equipo: ",equipo,"invalid type")                     