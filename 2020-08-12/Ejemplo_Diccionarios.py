# Defininicion de un Diccionario
diccionario = {
    # elemento : [valor]
    "perro": "chispita",
    "tortuga": "veloz",
    "gato" : "curioso",
    "lista" : ["esto","es","una","prueba"],
    "diccionario2" : {
        "activado" : True
    }
}

#Elemento perro
print(diccionario["perro"])

#Elemento Tortuga
print(diccionario["tortuga"])

#Elemento Gato
print(type(diccionario["gato"]))

#Lista dentro de Diccionario
print(diccionario["lista"][0])

#Diccionario dentro de Diccionario
print(diccionario["diccionario2"]["activado"])