#Definimos la lista

lista = ["Elemento 1", 2, ["Hola","Mundo"],[1,3]]

print(lista.count("Elemento 1"))
#print (dir(list))

# Primer Elemento
print(lista[0])

# Segundo Elemento
print(lista[1])

# Tercer Elemento 
print (lista[2])

# Tercer Elemento 0
print (lista[2][0])
# Tercer Elemento 1 
print (type(lista[2][1]))

# Cuarto Elemento
print (type(lista[3]))

# Cuarto Elemento 0 
print (lista[3][0])
# Cuarto Elemento 1
print (int(lista[3][1])+1)
