#Booleanos
Verdadero=  True
print(Verdadero)
print(type(Verdadero))
Falso=False
print(Falso)
print(type(Falso))

apagado=True #TRUE o FALSE

if apagado: #Si apagado==TRUE
    print("Esta Apagado")
else:
    print("Esta Encendido")

if not apagado: #Si apagado==FALSE
    print("Esta Encendido")
else:
    print("Esta Apagado")