#Armar un Scrip que reciba por teclado 3 lados de un triangulo e indicar,
#el tipo, perimetr y area.
#A= (Base*Altura)/2
#P= Lado_1+Lado_2+Lado_3
# Equilatero    -> Todos los lados iguales
# Escaleno      -> Todos los lados distintos
# Isoceles      -> Dos lados iguales, uno diferente

import math

Base=0.0
Altura=0.0

lado_01=0.0
lado_02=0.0
lado_03=0.0

print ("Ingrese 3 lados de un triangulo")
lado_01=input("Lado 01: ")
lado_02=input("Lado 02: ")
lado_03=input("Lado 03: ")

if lado_01==lado_02 and lado_01==lado_03:
    print ("El triangulo es Equilatero")

if lado_01!=lado_02 and lado_01!=lado_03 and lado_02!=lado_03:
    print ("El triangulo es Escaleno")

if lado_01==lado_02 and lado_01!=lado_03 or lado_02==lado_03 and lado_02!=lado_01:
    print ("El triangulo es Isoceles")

print ("El perimetro del triangulo es: ")
Perimetro= float(lado_01)+float(lado_02)+float(lado_03)
print (Perimetro)

print ("El area del triangulo es: ")

m = (float(lado_01)+float(lado_02)+float(lado_03))/2
Area = math.sqrt(m*(m-float(lado_01))*(m-float(lado_02))*(m-float(lado_03)))
    
print (Area)