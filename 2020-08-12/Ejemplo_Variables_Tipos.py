# Declaracion de Variables
numero=1
letra='a'
cadena="Hola"
cadena2="Mundo"
punto_flotante=0.2

# Impresion de Variables
print(numero)
print(type(numero))
print(letra)
print(type(letra))
print(cadena)
print(type(cadena))
print(punto_flotante)
print(type(punto_flotante))

# Concatenacion y suma de Variables
print(cadena+" "+cadena2) # Concatenar Cadenas
print(cadena+" "+letra)
print(numero+1) # Operaciones Numericas
print(punto_flotante+0.5) # Operaciones Numericas con Flotantes

# Cadena caso Especial
cadena3='"Hola!"' #Comillas dobles dentro de un String
print(cadena3)
cadena4="D'Alessandro" #Comillas simples dentro de un String
print(cadena4)

# Conversion de tipos de Variables
# Tipos int, float y String
cadenaNumero = "5"
print(int(cadenaNumero)+1)
print("Este es el numero "+str(cadenaNumero))