import os 
import random


class SuperHeroe:
    Lugares             = ["Gotham","La Rioja","Talampaya","Asgar","Coliseo","Smallville","New York","Akihabara","Kame House","Pueblo Paleta","Antares"]
    Superpoderes        = ["Tirar Fuego","Superfuerza","Invisibilidad","Garras de Acero","Velocidad","Teletransporte","Creacion de Objetos","Convertir en Animal","Manipular Agua","Volar","Ser Millonario"]
    Nombres             = ["Super","Man","Araña","Felino","Thunder","Estrella","Flash","Lizard","Muscle","Pocho","Pantera"]
    Situacion_monetaria = ["Pobre","Clase Media","Millonario","Billonario"]
    nombre              ="none"
    edad                = 0
    peso                = 0
    altura              = 0
    ciudad              = "none"
    poder_01            = "none"
    poder_02            = "none"
    poder_03            = "none"
    cantidad_dinero     = "none"
    
    def __init__(self):
        self.nombre                                 = self.generate_name()
        self.edad                                   = random.randrange(20, 50) 
        self.peso                                   = random.randrange(60, 80)
        self.altura                                 = random.randrange(160, 200)
        self.poder_01,self.poder_02,self.poder_03   = self.generate_power()
        self.ciudad                                 = self.generate_city()
        self.cantidad_dinero                        = self.generate_moneysituation()
        print ("Heroe Creado")
     
    def __str__(self):
        cadena= "Nombre: "+self.nombre+"\nEdad: "+str(self.edad)+"\nPeso: "+str(self.peso)+"\nAltura: "+str(self.altura)+"\nCiudad: "+self.ciudad+"\nPrimer Poder: "+self.poder_01+"\nSegundo Poder: "+self.poder_02+"\nTercer Poder: "+self.poder_03+"\nCantidad de Dinero: "+self.cantidad_dinero
        return (cadena)

    def generate_name(self):
        nombre=""
        selecion = random.randrange(0, len(self.Nombres))
        nombre =  self.Nombres[selecion]
        selecion = random.randrange(0, len(self.Nombres))
        nombre = nombre + self.Nombres[selecion]
        return (nombre)

    def generate_power(self):
        power_01=""
        power_02=""
        power_03=""
        
        while power_01==power_02 or power_01==power_03 or power_02==power_03:
            selecion = random.randrange(0, len(self.Superpoderes))
            power_01 =  self.Superpoderes[selecion]
            selecion = random.randrange(0, len(self.Superpoderes))
            power_02 =  self.Superpoderes[selecion]
            selecion = random.randrange(0, len(self.Superpoderes))
            power_03 =  self.Superpoderes[selecion]
        return (power_01,power_02,power_03)
    
    def generate_city(self):
        selecion = random.randrange(0, len(self.Lugares))
        return( self.Lugares[selecion])

    def generate_moneysituation(self):
        selecion = random.randrange(0, len(self.Situacion_monetaria))
        return( self.Situacion_monetaria[selecion])
    
    def get_name (self):
        return (self.nombre)
    def get_age (self):
        return (self.edad)
    def get_weight (self):
        return (self.peso) 
    def get_height (self):
        return (self.altura) 
    def get_city (self):
        return (self.ciudad)
    def get_power (self):
        return (self.poder_01,self.poder_02,self.poder_03) 
    def get_moneysituation (self):
        return (self.cantidad_dinero) 

    def edit_name(self,nombre=""):
        if (len(nombre)==0):
            self.nombre  = self.generate_name() 
        else:
            self.nombre  = nombre

    def edit_age (self,edad=0):
        if (edad==0):
            self.edad  = random.randrange(20, 50)
        else:
            self.edad  = edad
        
    def edit_weight(self,peso=0):
        if (peso==0):
            self.peso     = random.randrange(60, 80)
        else:
            self.peso     = peso

    def edit_height (self,altura=0):
        if (altura==0):
            self.altura   = random.randrange(160, 200)
        else:
            self.altura   = altura

    def edit_city (self,ciudad=""):
        if (len(ciudad)==0):
            self.ciudad   = self.generate_city() 
        else:
            self.ciudad   = ciudad

    def edit_power (self,poder_01="",poder_02="",poder_03=""):
        poder_01_new,poder_02_new,poder_03_new = self.generate_power()

        if (len(poder_01) == 0):
            self.poder_01 = poder_01_new
        else:
            self.poder_01 = poder_01
        
        if (len(poder_02) == 0):
            self.poder_02 = poder_02_new
        else:
            self.poder_02 = poder_02
        
        if (len(poder_03) == 0):
            self.poder_03 = poder_03_new
        else:
            self.poder_03 = poder_03  

    def edit_moneysituation (self,cantidad_dinero=""):
        if (len(cantidad_dinero) == 0):
            self.cantidad_dinero = self.generate_moneysituation()   
        else:
            self.cantidad_dinero = cantidad_dinero        

opcion_1=-1
MENU = ["01 - Crear Heroe","02 - Ver Heroe","00 - Salir"]

def clscr():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")                          


clscr()     
while opcion_1 != 0:
    print("Selecione una opcion")
    for elemento in MENU:
        print(elemento)
    selecion=input("Opcion: ")
    if (selecion.isnumeric() == False):
        print("Error: Debe ingresar un numero")
    else:
        opcion_1=int (selecion)
    if (opcion_1 < 0 or opcion_1 > len(MENU)-1):
        print ("Error: Valor ingresado incorrecto")
    else:
        if (opcion_1 == 0):
            clscr() 
            print("Muchas Gracias Vuelva Pronto")   
        if (opcion_1 == 1):
            clscr() 
            Heroe_01= SuperHeroe()
            print("\n")
        if (opcion_1 == 2):
            clscr() 
            print(Heroe_01)+
            
            print("\n")
