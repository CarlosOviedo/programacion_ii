#Alumno: Oviedo Carlos

#Enunciado: 
#El Director de un colegio necesita un sistema para poder contabilizar 
# la cantidad de alumnos de un curso, para ello requiere una herramienta que permita :
#1. Cargar alumnos con sus datos : Nombre, Apellido y DNI.
#2. Cargar los cursos con sus datos : Año, División.
#3. Permitir asignar alumnos a un Curso.
#4. Calcular la Cantidad de alumnos por curso y mostrarla.
#5. Listar alumnos por orden alfabético de un curso.

from ClassAlumno   import Alumno
from ClassCurso   import Curso

import os 
import random


opcion = -1

# Funcion para Limpiar Pantalla
def clscr():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")                          

# Funcion para Agregar un Nuevo Alumno
def add_alumno():
    nombre      = ""
    apellido    = ""
    dni         = ""
    edad        = ""

    print ("### NUEVO ALUMNO ####")
    while len(nombre)== 0:
        nombre = input("Nombre: ")
        if len(nombre)== 0:
            print ("Error: el nombre no puede estar vacio")
    
    while len(apellido)== 0:
        apellido = input("Apellido: ")
        if len(apellido)== 0:
            print ("Error: el Apellido no puede estar vacio")
            apellido = ""

    while len(dni)== 0:
        dni = input("DNI: ")
        if len(dni)== 0:
            print ("Error: el DNI no puede estar vacio")
        if dni.isnumeric == False:
            print ("Error: el DNI debe ser un numero")
            dni = ""

    while len(edad)== 0:
        edad = input("Edad: ")
        if len(edad)== 0:
            print ("Error: la EDAD no puede estar vacio")
        if edad.isnumeric == False:
            print ("Error: la EDAD debe ser un numero")    
            edad = ""

    alumno_new = Alumno (nombre,apellido,int(dni),int(edad))
    return(alumno_new)

# Funcion para Agregar un Nuevo Curso
def add_curso ():
    print ("### NUEVO CURSO ####")
    anio=0
    
    while anio == 0 or anio<0 :
        aux = input("Ingrese Año: ")
        if len(aux)== 0:
            print ("Error: el Año no puede estar vacio")
        if (aux.isdigit()):
            anio = int(aux)
        if anio == 0 or anio<0 :
            print("Error: Numero invalido")
    
    division=0
    while division == 0 or division<0 :
        aux = input("Ingrese Division: ")
        if len(aux)== 0:
            print ("Error: Division no puede estar vacio")
        if (aux.isdigit()):
            division = int(aux)
        if division == 0 or division<0 :
            print("Error: Numero invalido")    

    curso_new = Curso(anio,division)
    return (curso_new)

# Funcion para Buscar un Curso
def serch_curso():
    index_curso=int (-1)
    print ("Seleccione el Curso")
    print ("Index: \tAño:\tDivision:")            
    for i in range(0,len(lista_cursos)):
        print (str(i),"\t",lista_cursos[i].Anio(),"\t",lista_cursos[i].Division())
    
    while index_curso<0 or index_curso>i:
        index_curso=int(input("Ingrese Index: "))
        if index_curso>i or index_curso<0 :
            print ("Error: Index Invalido")        

    return(index_curso)

# Funcion para Buscar un Alumno
def serch_alumno():
    index_alumno=int (-1)
    print ("Seleccione el Alumno")
    print ("Index: \tApellido:\tNombre:") 
    for i in range(0,len(lista_alumnos)):
        print (str(i),"\t",lista_alumnos[i].Nombre(),"\t",lista_alumnos[i].Apellido())
    while index_alumno<0 or index_alumno>i:
        index_alumno=int(input("Ingrese Index: "))
        if index_alumno>i or index_alumno<0 :
            print ("Error: Index Invalido")
    return(index_alumno)

menu = '''### MENÚ ###
- 1 Crear Curso
- 2 Crear Alumno
- 3 Agregar Alumno al Curso
- 4 Ver Curso
- 5 Cantidad de Alumnos por Curso
- 6 Listar alumnos
- 7 Salir'''
lista_alumnos=[]
lista_cursos=[]
#Funcion Principal
while opcion!=7:
    print (menu)
    selecion=input("Opcion: ")
    if (selecion.isnumeric() == False):
        print("Error: Debe ingresar un numero")
    else:
        opcion=int (selecion)
    if (opcion < 1 or opcion > 7):
        print ("Error: Valor ingresado incorrecto")
    else:
# Opcion:  Salir 
        if (opcion == 7):
            print("Muchas Gracias Vuelva Pronto") 
# Opcion:  Crear Curso            
        elif opcion == 1:
            nuevo_curso = add_curso ()
            lista_cursos.append(nuevo_curso)
# Opcion:  Crear Alumno
        elif opcion == 2:
            nuevo_alumno=add_alumno()
            lista_alumnos.append(nuevo_alumno)
# Opcion:  Agregar Alumno al Curso 
        elif opcion == 3:
            indexcurso=serch_curso()
            clscr() 
            indexalumno=serch_alumno()
            clscr()

            print("Curso: ",str(lista_cursos[indexcurso].Anio()),"-",str(lista_cursos[indexcurso].Division())) 
            print("Alumno: ",lista_alumnos[indexalumno].Apellido()," ",lista_alumnos[indexalumno].Nombre()) 
            lista_cursos[indexcurso].Agregar_estudiante(lista_alumnos[indexalumno])
# Opcion:  Ver Curso        
        elif opcion == 4:
            index_curso=serch_curso()
            clscr()    
            print ("Curso: ",str(lista_cursos[index_curso].Anio()),"\nDivisio: ",str(lista_cursos[index_curso].Division())) 
            lista_cursos[index_curso].Ver()
# Opcion:  Cantidad de Alumnos por Curso         
        elif opcion == 5:
            index_curso=serch_curso()
            clscr()
            print ("Curso: ",str(lista_cursos[index_curso].Anio()),"\nDivisio: ",str(lista_cursos[index_curso].Division()),"\nTotal de Alumnos: ",str(lista_cursos[index_curso].Total()))
# Opcion:  Listar alumnos      
        elif opcion == 6:
            index_curso=serch_curso()
            clscr()            
            print ("Index \t Orden\n0 \t A-Z\n1 \t Z-A ")
            idex_orden= input("Ingrese numero:")
            if (idex_orden=="0"):
                lista_cursos[index_curso].Ordenar(False)
                clscr()
                print ("Curso: ",str(lista_cursos[index_curso].Anio()),"\nDivisio: ",str(lista_cursos[index_curso].Division())) 
                lista_cursos[index_curso].Ver()
            elif (idex_orden=="1"): 
                lista_cursos[index_curso].Ordenar(True) 
                clscr() 
                print ("Curso: ",str(lista_cursos[index_curso].Anio()),"\nDivisio: ",str(lista_cursos[index_curso].Division()))
                lista_cursos[index_curso].Ver()               
            elif (idex_orden!="1" and idex_orden=="0"): 
                print("Eror: Ingreso invalido")          
