from ClassAlumno   import Alumno
class Curso:
    anio=0
    division=0
    
    def __init__(self,anio,division):
        self.lista=[]
        self.cnt=0
        if isinstance (anio,int):
            self.anio = anio
        else:
            print ("Error: ",anio,"invalid type")                     
        
        if isinstance (division,int):
            self.division = division
        else:
            print ("Error: ",division,"invalid type")                            

        print ("Se creo el Curso: ",self.anio,"-",self.division)
    
    def Agregar_estudiante(self,estudiante):
        if type(estudiante) == Alumno:
            nombre   = estudiante.Nombre()
            apellido = estudiante.Apellido()
            nombre_completo = apellido + " " + nombre
            self.lista.append(nombre_completo)
            self.cnt=self.cnt+1 
            print ("Se agrego: ",nombre_completo, "al curso: ",self.anio,"-",self.division)        
        else:
            print("No es un estudiante")

    def Total (self):
        return (self.cnt)

    def Ordenar(self,sentido):
        if isinstance (sentido,bool):
            if sentido==True:
                self.lista = sorted(self.lista,reverse=sentido) 
            else:
                self.lista = sorted(self.lista,reverse=sentido)         
        else:
           print ( "Error: ",sentido,"invalid type") 

    def Ver(self):
        print ("Index \tApellido Nombre")
        i=0
        for estd in self.lista:
            print (str(i),"\t",estd)
            i=i+1

    def Anio (self):
        return (self.anio)
    def Division (self):
        return (self.division)        