#   FASE DIOS

import winsound 
import time

#Definicion de FUNCIONES
# FUNCION MUSICA
def music ():
    winsound.Beep(300, 500);
    time.sleep(0.25); 
    winsound.Beep(300, 500);
    time.sleep(0.25);
    winsound.Beep(300, 500);
    time.sleep(0.25);
    winsound.Beep(250, 500);
    time.sleep(0.050);
    winsound.Beep(350, 250);
    winsound.Beep(300, 500);
    time.sleep(0.25);
    winsound.Beep(250, 500);
    time.sleep(0.050);
    winsound.Beep(350, 250);
    winsound.Beep(300, 500);
    time.sleep(0.050);

# FUNCION Codificador
def Codificador (palabra):
    clave="MURCIELAGO"
    codigo =""
    for i in range(0,len(palabra),1):
        if(palabra[i] not in clave):
            codigo+=palabra[i]
        elif(palabra[i] in clave):
            for j in range (0,len(clave),1):
                if(palabra[i]==clave[j]):
                    codigo+=str(j)
    return (codigo)                         

# FUNCION Decodificador
def Decodificador (codigo):
    numeros = "0123456789"
    clave   = "MURCIELAGO"
    palabra = ""

    for i in range(0,len(codigo),1):
        if(codigo[i] not in numeros):
            palabra+=codigo[i]
        elif(codigo[i] in numeros):
            for j in range (0,len(clave),1):
                if(int (codigo[i])==j):
                    palabra+=clave[j]
    music()
    return (palabra)         

# PROGRAMA PRICIPAL

flag=False
opcion = "1"

# MENU
while flag !=True:
    print("1 - Ingrese una Palabra para Codificar.")
    print("2 - Ingrese codigo para decodificar.")
    opcion=input ("Ingrese una Opcion:")
    if(opcion =="1" or opcion =="2"):
        flag = True
    if(opcion !="1" and opcion !="2"):
        print("ERROR! OPCION INCORRECTA")

if(opcion=="1"):
    palabra=input("Ingrese una palabra: ")
    palabra=palabra.upper()
    palabra_codificada=Codificador (palabra)
    print("La Palabra codificada es: " + palabra_codificada)

elif (opcion=="2"):
    codigo=input("Ingrese un codigo: ")
    palabra_decodificada=Decodificador(codigo)
    print("La Palabra codificada es: " + palabra_decodificada)