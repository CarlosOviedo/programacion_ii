# FASE ULTRA INSTINTO

import random

# FUNCION Generadora de Password
def pass_generation ():
    vocales="aeiou"
    caracter=""
    password=""
    for i in range (0,11,1):
        if i%2 == 0:
            while caracter in vocales:
                caracter = chr(random.randrange(97,122))
        if i%2 != 0:
            while caracter not in vocales:
                caracter = chr(random.randrange(97,122))
        if i==8:
            caracter = "_"
        if i==9:
            caracter = str(random.randrange(0,9,1))
        if i==10:
            caracter = str(random.randrange(0,9,1))
        password+=caracter
    password=password.capitalize()
    return (password)

# Funcion Principal
contraseñas = [] 
flag=False
opcion = "1"
print ("Generacion de 20 Contraseñas: ")
while flag !=True:
    
    print (":::::::::::::::MENU:::::::::::::::::")
    print ("1. - Generar Contraseñas")
    print ("2. - Mostrar Contraseñas")
    print ("3. - SALIR")
    opcion=input ("Ingrese una Opcion: ")
    if(opcion !="1" and opcion !="2" and opcion !="3"):
        print("ERROR! OPCION INCORRECTA")

    if(opcion=="1"):
        print("Se Crearon 20 Contraseñas")
        for i in range (0,20,1):
            contraseñas.append(pass_generation ())
    elif (opcion=="2"):
        print("Las Contraseñas son:")
        for i in range(0,20,1):
            print (str (i) + ":\t" + contraseñas[i])
        flag = True
    elif (opcion=="3"):
        flag = True
