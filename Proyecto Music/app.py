#Clases Usadas:
from clases.Classcancion       import Cancion
from clases.Classcanciones     import Lista
from datetime import date


#Inicializacion de Variables:
today = date.today()

opcion          = True

mis_canciones    = []
mis_listas       = []
lista_encontrada = []
tema_encontrado  = []

menu = '''### MENÚ ###
- 1 Agregar Canción
- 2 Crear una Lista
- 3 Agregar Canción a Lista
- 4 Quitar Canción de Lista
- 5 Buscar
- 6 Salir'''

# Funcion para Agregar a Listas
def add_lista():
    nombre = ""
    # Validacion de Nombre de la Lista ...
    while len (nombre) == 0:
        nombre = input("Nombre de la Lista\n")
        if len (nombre) == 0:
            print ("Error: Nombre de la lista incorrecto.")    
    
    mis_listas.append(Lista(nombre))
    print("Se creó la Lista "+mis_listas[len(mis_listas)-1].nombre)
    #Devuelvo el índice de lo insertado
    return len(mis_listas)-1

# Funcion para Agregar una Cancion
def add_cancion():
    titulo      = ""
    interprete  = ""
    duracion    = ""
    anio        = " "
    # Validacion de Titulo
    while len (titulo) == 0:
        titulo      = input("Título\n")
        if len (titulo) == 0:
            print ("Error: la cancion debe poseer un titulo.")
    # Validacion de Interprete        
    while len (interprete) == 0:
        interprete  = input("Interprete\n")
        if len (interprete) == 0:
            print ("Error: la cancion debe poseer un interprete.")    
    
    # Validacion de Duracion
    while len (duracion) == 0 or duracion == "0" or duracion.isalpha()== True:
        duracion = input("Duración \n")
        if (duracion.isalpha()== True ):
            print ("Error: la duracion debe ser un numero")
        if len (duracion) == 0 or duracion == "0":
            print ("Error: la cancion debe poseer un duracion distinta de 0.")            
    genero = input("Género \n")
    if len (genero) == 0:
        genero = "none"
    #Validacion del Año 
    while anio.isnumeric()== False :
        anio = input("Año \n")
        if len (anio) == 0:
            anio = str(today.year)
        if (anio.isnumeric()== False ):
            print ("Error: la año debe ser un numero")
    sello       = input("Sello \n")
    if len (sello) == 0:
        sello = "none"        
    album       = input("Album \n")
    if len (album) == 0:
        album = "none"  
    cancion     = Cancion(titulo,interprete,float(duracion),genero,anio,sello,album)
    mis_canciones.append(cancion)
    print("Se creo la siguiente cancion:", mis_canciones[len(mis_canciones)-1])    

# Funcion para Busqueda de Tema.
def serch_cancion(titulo_abuscar,interprete_abuscar):
    lista_find=[]
    tema_find =[]
    for i in range (0,len(mis_listas)):
        for j in range (0,len(mis_listas[i].listado_canciones)):
            if ((titulo_abuscar == mis_listas[i].listado_canciones[j].titulo) and (interprete_abuscar == mis_listas[i].listado_canciones[j].interprete)):
                lista_find.append(i)
                tema_find.append(j)
    if len (lista_find) == 0:
        print("No se encontro la cancion: ",titulo_abuscar," - ",interprete_abuscar)
    else:
        print ("La Cancion: ",titulo_abuscar," - ",interprete_abuscar," se encontro en las siguientes listas:")
        print ("Index: \t-\tLista:")
        for item in lista_find:
            print(item,"\t-\t",mis_listas[item].nombre)
    
    return lista_find, tema_find

while opcion == True :
    op          = 0

    print(menu)
    # Validacion de las opciones del Menu ... 
    while op > 6 or op < 1:
        op = int (input("Ingrese una opcion\n"))
        if op > 6 or op < 1:
            print ("Error: Opcion Invalida.")
    
    if op == 1:
        add_cancion()

    elif op == 2:
        add_lista()

    elif op == 3: 
        #Validacion de Temas
        if len(mis_canciones) == 0:
            rsp = input("Error: No se cargo ninguna Cancion. ¿Quiere cargar una cancion? (S/N): ")
            if(rsp=="S"):
                add_cancion()
            else:
                break
        #Validacion de Listas.
        if len(mis_listas) == 0:
            rsp = input("Error: No se cargo ninguna Lista. ¿Quiere cargar una Lista? (S/N): ")
            if(rsp=="S"):
                add_lista()
            else:
                break
        # LISTA DE CANCIONES    
        print(" ###  LISTA DE CANCIONES ###\n")
        print("Index:\t-\tTitulo:")
        for cancion in mis_canciones:
            print(str(mis_canciones.index(cancion))+"\t-\t"+cancion.titulo+"\n")
            
        print("Ingrese el index de las canciones que quiera agregar a una lista \n")
        print("(Separadas por comas)\n")
        canciones = input("Index de la/las cancion/canciones: ")
        canciones_split = canciones.split(",")

        #Damos la opción de agregar una lista nueva o utilizar las cargadas
        nueva_lista = input("¿Quiere cargar una lista nueva? S/N \n")
        if(nueva_lista=="S"):
            listaIndex = add_lista()
        else:
            print("SELECCIONE UNA LISTA \n")
            print("Index:\t-\tLista:")
            for lista in mis_listas:
                print(str(mis_listas.index(lista))+"\t-\t"+lista.nombre+"\n")
            listaIndex = input("Index Lista: ")
        #Agregar las canciones del split a la lista ingresada o seleccionada
        print ("En " + mis_listas[int(listaIndex)].nombre + ":")

        for cancionIndex in canciones_split:
            mis_listas[int(listaIndex)].agregar(mis_canciones[int(cancionIndex)])

    elif op == 4:
        titulo_aborrar      = ""
        interprete_aborrar  = ""
        # Primero veo si tengo canciones creadas y listas creadas.
        if len(mis_canciones) != 0 and len(mis_listas) != 0:
        
            # Validacion de Titulo
            while len (titulo_aborrar ) == 0:
                titulo_aborrar = input("Ingrese Título\n")
                if len (titulo_aborrar ) == 0:
                    print ("Error: la cancion debe poseer un titulo.")
            # Validacion de Interprete        
            while len (interprete_aborrar) == 0:
                interprete_aborrar  = input("Interprete\n")
                if len (interprete_aborrar) == 0:
                    print ("Error: la cancion debe poseer un interprete.") 
            # Busqueda de Tema y Lista a Borrar.
            lista_encontrada , tema_encontrado = serch_cancion(titulo_aborrar,interprete_aborrar)
            listaIndex=input ("Selecione la lista de donde decea borrar el tema: ")
            mis_listas[int (listaIndex)].quitar(titulo_aborrar,interprete_aborrar)
            #Borramos la lista luego de usarla
            lista_encontrada.clear()
            tema_encontrado.clear()
        
        else:
            if len(mis_canciones) == 0:
                print ("Error: No existe ninguna cancion cargada")
            if len(mis_listas) == 0:
                print ("Error: No existe ninguna lista cargada")

                
    elif op == 5:
        titulo_abuscar      = ""
        interprete_abuscar  = ""
        if len(mis_canciones) != 0 and len(mis_listas) != 0:
            # Validacion de Titulo
            while len (titulo_abuscar ) == 0:
                titulo_abuscar = input("Ingrese Título\n")
                if len (titulo_abuscar ) == 0:
                    print ("Error: la cancion debe poseer un titulo.")
            # Validacion de Interprete        
            while len (interprete_abuscar) == 0:
                interprete_abuscar  = input("Interprete\n")
                if len (interprete_abuscar) == 0:
                    print ("Error: la cancion debe poseer un interprete.")             
            serch_cancion(titulo_abuscar,interprete_abuscar)


        
        else: 
            if len(mis_canciones) == 0:
                print ("Error: No existe ninguna cancion cargada")
            if len(mis_listas) == 0:
                print ("Error: No existe ninguna lista cargada")        
    elif op == 6:
        opcion=False

