from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana                   (QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-26/ventana2.ui",self)                                #Ubicamos la ventana realizado con "designed"
        self.boton_sumar.released.connect(self.on_click)
        self.boton_sumar.pressed.connect(self.on_pressed)
   
    def on_click(self):
        numero_01   =   self.numero1.text()
        numero_02   =   self.numero2.text()
        resultado   =   float (numero_01)+float(numero_02)
        self.resultado.setText(str(resultado))
        print ("Levantado")
        
    
    def on_pressed(self):
        print ("Presionado")        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  