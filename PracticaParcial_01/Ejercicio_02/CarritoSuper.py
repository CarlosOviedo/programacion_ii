import uuid
from random import randint

class Producto:
    def __init__(self,descripcion,codigoBarras,precio,proveedor):
        self.id = uuid.uuid4()
        self.descripcion = descripcion
        self.clave = randint(1,200)
        self.codigoBarras = codigoBarras
        self.precio = precio
        self.proveedor = proveedor
    def __str__(self):
        return '{0}: {1} | {2} | {3} | {4} | {5}'.format(self.id,self.descripcion,self.clave,self.codigoBarras,self.precio,self.proveedor)

class Carrito:
    def __init__(self):
        self.listadoProductos = []
        self.usuario = ""
    def cargarProducto(self,prod,cant):
        self.listadoProductos.append([prod,cant])
    def mostrarProductos(self):
        j=0
        for i in range(0,len(self.listadoProductos)):
            print(str(i+1) + " - " + str(self.listadoProductos[i][0].descripcion) +"\t"+ str(self.listadoProductos[i][1])  + "\n")
            j=i
        return (j)
    def QuitarProducto(self,index,cnt):
        aux=self.listadoProductos[index][1]
        if(aux-cnt<=0):
            del(self.listadoProductos[index])
        else:
            self.listadoProductos[index][1]=aux-cnt
        print("En Proces....")


class ListaProductos:
    def __init__(self):
        self.listadoProductos = []
    def cargarProducto(self,prod):
        self.listadoProductos.append(prod)
    def mostrarProductos(self):
        i = 0
        for Producto in self.listadoProductos:
            print(str(i) + " - " + str(Producto.descripcion) + "\n")
            i=i+1
        return (i)     
# Manzana = Producto("Fruta",1231241231,120,"Moño Azul")
# Carrito1 = Carrito()
# Carrito1.cargarProducto(Manzana,2)
# print(Carrito1.listadoProductos[0][0].descripcion)
# print(Carrito1.listadoProductos[0][1])
# print(Carrito1.listadoProductos)

menu = '''### MENÚ ###
- 1 Agregar Producto
- 2 Agregar al Carrito
- 4 Ver Carrito
- 5 Quitar Producto de Carrito 
- 6 Salir'''

opcion = True
listadoProductosObjeto = ListaProductos()
carritoProductosObjeto = Carrito()

while opcion == True :
    print(menu)
    op = int (input("Ingrese una Opción\n"))
    if op == 1:
        descripcion=""
        while len(descripcion)== 0:
            descripcion = input("Descripcion\n")
            if len(descripcion)== 0:
                print ("Error: la Descripcion no puede estar vacio")
       
        codigoBarras = 0
        while codigoBarras == 0:
            aux = input("Codigo de Barras\n")
            if len(aux)== 0:
                print ("Error: Codigo de Barras estar vacio")
            if (aux.isdigit()):
                codigoBarras = int(aux)
                if descripcion == 0:
                    print ("Error: Codigo de Barras no puede ser cero")                
                
            else: 
                print ("Error: Codigo de Barras debe ser un numero entero")   

        precio = 0
        while precio == 0:
            precio = int (input("Precio\n"))
            if precio == 0:
                print ("Error: Precio no puede ser cero")
        
        
        proveedor=""
        while len(proveedor)== 0:
            proveedor = input("Proveedor\n")
            if len(proveedor)== 0:
                print ("Error: el proveedor no puede estar vacio")

        
        objetoTransitorio = Producto(descripcion, codigoBarras, precio, proveedor)
        listadoProductosObjeto.cargarProducto(objetoTransitorio)
        print("Se agrego el Producto",objetoTransitorio)
        #listadoProductosObjeto(Producto(descripcion,codigoBarras,precio,proveedor))
    elif op == 2:
        i=listadoProductosObjeto.mostrarProductos()
        indice = -1
        while indice < 0:
            aux = input("Ingrese el numero del producto\n")
            if len(aux)== 0:
                print ("Error: indice vacio")
            if (aux.isdigit()):
                indice = int(aux)
                if indice < 0:
                    print ("Error: indice no puede ser menor a cero")                
                if indice > (i-1):
                    print ("Error: numero incorrecto")
                    indice=-1
            else: 
                print ("Error: indice debe ser un numero entero")  
                        
        cantidad = 0
        while cantidad == 0:
            aux = input("cantidad\n")
            if len(aux)== 0:
                print ("Error: cantidad vacio")
            if (aux.isdigit()):
                cantidad = int(aux)
                if cantidad <= 0:
                    print ("Error: cantidad no puede ser menor a cero")                
                
            else: 
                print ("Error: cantidad debe ser un numero entero")  

        productoTransitorio = listadoProductosObjeto.listadoProductos[indice]
        carritoProductosObjeto.cargarProducto(productoTransitorio,cantidad)

    elif op == 4:
        print ("Productos \tCantidad")
        carritoProductosObjeto.mostrarProductos()

    elif op == 6:
        opcion=False

