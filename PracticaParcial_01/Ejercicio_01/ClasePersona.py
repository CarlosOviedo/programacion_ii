from datetime import date
class persona:
    nombre      = "none"
    apellido    = "none"
    dni         = 0
    edad        = 0
    fech_nac    = date.today()
    mail        = "none@none.com"
    telefono    = "0000"
    def __init__(self,nombre,apellido,dni,edad,fech_nac,mail,telefono):
        if isinstance (nombre,str):
            self.nombre = nombre
        else:
           print ( "Error: ",nombre,"invalid type") 

        if isinstance (apellido,str):
            self.apellido = apellido
        else:
            print ("Error: ",apellido,"invalid type")                    
        
        if isinstance (dni,int):
            self.dni = dni
        else:
            print ("Error: ",dni,"invalid type")                     
        
        if isinstance (edad,int):
            self.edad = edad
        else:
            print ("Error: ",edad,"invalid type")                            
        
        if isinstance (fech_nac,str):
            fecha=fech_nac.split('-')
            self.fech_nac = date(int(fecha[0]), int(fecha[1]), int(fecha[2]))
        else:
            print ("Error: ",fech_cumple,"invalid type")                      
    
        if isinstance (mail,str):
            self.mail = mail
        else:
            print ("Error: ",mail,"invalid type")                       
        
        if isinstance (telefono,str):
            self.telefono = telefono
        else:
            print ("Error: ",telefono,"invalid type")
        print ("Se creo el objeto: ",self.nombre,"-",self.apellido)
# Metodos Especiales :
    # Metodos para comparar :
    def __eq__(self, otro): #Si de la misma edad
        if self.edad == otro.edad:
            return True
        else:
            return False

    def __lt__(self, otro): #Si es menor de edad
        if self.edad < otro.edad:
            return True
        else:
            return False

    def __gt__(self, otro): #Si es mayor de edad
        if self.edad > otro.edad:
            return True
        else:
            return False

    def __str__ (self):
        cadena = self.nombre+", " + self.apellido+", " +str(self.dni)+", " +str (self.edad)+", " +str(self.fech_nac)+", " +self.mail+", " +self.telefono
        return (cadena)

#   def __del__ (self):
#       print ("Se elimino: "+self.nombre+", " + self.apellido+", " +str(self.dni)+", " +str (self.edad)+", " +str(self.fech_nac)+", " +self.mail+", " +self.telefono)                                 

# Metodos Propios :

    def Nombre (self):
        return (self.nombre)
    def Apellido (self):
        return (self.apellido)  
    def Dni (self):
        return (self.dni)  
    def Edad (self):
        return (self.edad)
    def Nacimiento (self):
        return (str(self.fech_nac))
    def Mail (self):
        return (str(self.mail))                                         
    def Telefono (self):
        return (str(self.telefono))  

    def Modificar_Nombre (self,nombre):
        if isinstance (nombre,str):
            self.nombre = nombre
        else:
           print ( "Error: ",nombre,"invalid type")  
    def Modificar_Apellido (self,apellido):    
        if isinstance (apellido,str):
            self.apellido = apellido
        else:
            print ("Error: ",apellido,"invalid type")                    
    def Modificar_Dni (self,dni):     
        if isinstance (dni,int):
            self.dni = dni
        else:
            print ("Error: ",dni,"invalid type")                     
    def Modificar_Edad (self,edad):      
        if isinstance (edad,int):
            self.edad = edad
        else:
            print ("Error: ",edad,"invalid type")                            
    def Modificar_Nacimiento (self,fech_nac):    
        if isinstance (fech_nac,str):
            fecha=fech_nac.split('-')
            self.fech_nac = date(int(fecha[0]), int(fecha[1]), int(fecha[2]))
        else:
            print ("Error: ",fech_cumple,"invalid type")                      
    def Modificar_Mail (self,mail):
        if isinstance (mail,str):
            self.mail = mail
        else:
            print ("Error: ",mail,"invalid type")                       
    def Modificar_Telefono (self,telefono):    
        if isinstance (telefono,str):
            self.telefono = telefono
        else:
            print ("Error: ",telefono,"invalid type")