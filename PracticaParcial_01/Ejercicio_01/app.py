from ClasePersona   import persona
from ClaseAgenda    import agenda
# Variables Globales:
Lista_agendas=[]
Menu = '''
Elija una opcion:
       1. - Crear una nueva Agenda.
       2. - Crear un contacto.
       3. - Lista de Agendas.
       4. - Visualizar Agenda.
       5. - Buscar un Contacto.
       6. - Eliminar un Contacto.
       0. - Salir
       '''
opcion = -1

# FUNCIONES:
def add_agenda():
    nombre      = ""
    print ("### NUEVA AGENDA ####")
    while len(nombre)== 0:
        nombre = input("Nombre: ")
        if len(nombre)== 0:
            print ("Error: el nombre no puede estar vacio")
    agenda_new = agenda(nombre) 
    return(agenda_new)

def add_contacto():
    nombre      = ""
    apellido    = ""
    dni         = ""
    edad        = ""
    Anio        = ""
    mes        = ""
    dia        = ""        
    mail        = ""
    Telefono    = ""

    print ("### NUEVO CONTACTO ####")
    while len(nombre)== 0:
        nombre = input("Nombre: ")
        if len(nombre)== 0:
            print ("Error: el nombre no puede estar vacio")
    
    while len(apellido)== 0:
        apellido = input("Apellido: ")
        if len(apellido)== 0:
            print ("Error: el Apellido no puede estar vacio")
            apellido = ""

    while len(dni)== 0:
        dni = input("DNI: ")
        if len(dni)== 0:
            print ("Error: el DNI no puede estar vacio")
        if dni.isnumeric == False:
            print ("Error: el DNI debe ser un numero")
            dni = ""

    while len(edad)== 0:
        edad = input("Edad: ")
        if len(edad)== 0:
            print ("Error: la EDAD no puede estar vacio")
        if edad.isnumeric == False:
            print ("Error: la EDAD debe ser un numero")    
            edad = ""

    while len(Anio)== 0:
        Anio = input("Año de Nacimiento: ")
        if len(Anio)== 0:
            print ("Error: el Año no puede estar vacio")
        if Anio.isnumeric == False:
            print ("Error: el Año debe ser un numero")
            Anio = ""

    while len(mes)== 0:
        mes = input("Mes de Nacimiento: ")
        if len(mes)== 0:
            print ("Error: el mes no puede estar vacio")
        if mes.isnumeric == False:
            print ("Error: el mes debe ser un numero")
            mes = ""

    while len(dia)== 0:
        dia = input("Dia de Nacimiento: ")
        if len(dia)== 0:
            print ("Error: el dia no puede estar vacio")
        if dia.isnumeric == False:
            print ("Error: el dia debe ser un numero")
            dia = ""

    fech_nac= Anio + "-" + mes + "-" + dia

    while len(mail)== 0:
        mail = input("Mail: ")
        if len(mail)== 0:
            print ("Error: el mail no puede estar vacio")

    while len(Telefono)== 0:
        Telefono = input("Telefono: ")
        if len(Telefono)== 0:
            print ("Error: el Telefono no puede estar vacio")
        if Telefono.isnumeric == False:
            print ("Error: el Telefono debe ser un numero")
            Telefono = ""
    
    contacto_new = persona (nombre,apellido,int(dni),int(edad),fech_nac,mail,Telefono)
    return(contacto_new)

def agendas_seleccion():
    seleccion = ""
    print("Seleccione una Agenda: ")
    print("ID \t\t Nombre:")
    for i in range (0,len(Lista_agendas)):
        print(str(i),"\t\t",Lista_agendas[i].nombre() )
    
    while len(seleccion)== 0:
        seleccion= input("Agenda: ") 
        if len(seleccion)== 0:
            print ("Error: debe elegir una agenda")
        if seleccion.isnumeric == False:
            print ("Error: debe ser un numero")
            seleccion = ""    
    return (int (seleccion))

# Funcion Principal:
while opcion!=0:
    print (Menu)
    selecion=input("Opcion: ")
    if (selecion.isnumeric() == False):
        print("Error: Debe ingresar un numero")
    else:
        opcion=int (selecion)
    if (opcion < 0 or opcion > 5):
        print ("Error: Valor ingresado incorrecto")
    else:
        if (opcion == 0):
            print("Muchas Gracias Vuelva Pronto")   
        if (opcion == 1):
           agenda = add_agenda() 
           Lista_agendas.append(agenda)
        if (opcion == 2):
            if len (Lista_agendas) == 0:
                print ("Debe crear una agenda primero")
            else:
                contacto= add_contacto()
            print ("Seleccionar una Agenda: ")
                                  
        if (opcion == 3):
            print ("Agendas Creadas:")
            for element in Lista_agendas:
                print (element.nombre()) 

        if (opcion == 4):
            print ("Visualizar Agenda")
            agenda_selecionada=agendas_seleccion() 
             Lista_agendas[agenda_selecionada].mostrar_agenda()            
        if (opcion == 5):
            print ("Buscar un Contacto")
        if (opcion == 6):
            print("Eliminar un Contacto")                                                        