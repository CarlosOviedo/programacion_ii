from ClasePersona import persona
class agenda:
    nombre ="none"
    lista_personas = []
    
    def __init__(self,nombre):
        if isinstance (nombre,str):
            self.nombre=nombre
        else:
            print ("Error: ",nombre,"invalid type")             
    def nombre (self):
        return (self.nombre)

    def agregar (self,contacto):
        if type(contacto) == persona:
            self.lista_personas.append(contacto)
            print("Se agregó el contacto : "+ contacto.Nombre() +", "+ contacto.Apellido())
        else:
            print ("Error: ",contacto,"invalid type")  

    def mostrar_agenda(self):
        print("Nombre \t\t\t Apellido \t\t\t DNI \t\t\t Telefono")        
        for element in self.lista_personas:
            print(element.Nombre(),end=" ")
            x= int((30-len(element.Nombre()))/8)
            for y in range(0,x,1):
                print ("\t",end=" ")
            print(element.Apellido(),end=" ")
            x= int((30-len(element.Apellido()))/8)
            for y in range(0,x,1):
                print ("\t",end=" ")  
            print(element.Dni(),end=" ")                
            x= int((30-len(str(element.Dni())))/8)
            for y in range(0,x,1):
                print ("\t",end=" ")            
            print(element.Telefono())

    def buscar_contacto(self,dni):
        if isinstance (dni,int):
            for element in self.lista_personas:
                if(element.Dni() == dni):
                    print ("Nombre:         ",element.Nombre())
                    print ("Apellido:       ",element.Apellido())
                    print ("DNI:            ",element.Dni())
                    print ("Fecha de Nac:   ",element.Nacimiento())
                    print ("Edad:           ",element.Edad())
                    print ("Telefono:       ",element.Telefono())
                    print ("Mail:           ",element.Mail())
        else:
            print ("Error: ",dni,"invalid type")

    def quitar(self,dni):
        if isinstance (dni,int):
            for element in self.lista_personas:
                if(element.Dni() == dni):
                    self.lista_personas.remove(element)
                    print ("Se elimino: ",element.Nombre()," - ",element.Apellido())  
         