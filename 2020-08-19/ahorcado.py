# 1) Que se elija una palabra del listado
#    y se muestre los respectivo guiones.
# 2) Que el jugador ingrese una letra y se muestre
#    en el lugar de los guiones o que baje la vida.
# 3) Tenga 3 vidas del jugador.
# 4) Un diccionario de palabras (Lista)
#    palbras=["perro","manzana","reja","luz","negro", "cortina", "juego"].
# 5) Cuando las vidas sean 0 mostrar GAME OVER.
# 6) Cuando se adivine mostrar You WIN.
# 7) No se puede ingresar dos veces la misma letra.

import random
import os


# Palabras del Juego
palabras=["perro","manzana","reja","luz","negro", "cortina", "juego"]
palabra_adivinada=" "
# Vidas
vida = 5
flag_win=False

# Variable Auxiliar
guiones = "_"

# Seleccion Aleatoria
seleccion= random.randrange(0, len(palabras), 1)
# Palabras Seleccionada
palabra_seleccion   = palabras[seleccion]
palabra_largo       = len(palabras[seleccion])
palabra_adivinada   = palabras[seleccion]

print ("El Juego del AHORCADO")
print ("Descubra la siguiente palabra: " + palabra_seleccion)
print ("La palabra seleccionada tiene: " + str(palabra_largo) + " letras")

while vida > 0:
    contador = 0
    print("Introduce una letra: ")
    letra = input() # pedimos al usuario una letra#
    
    for i in range(0,palabra_largo,1): 
        if(letra==palabra_seleccion[i]):
            posicion=i


    if letra not in palabra_seleccion: # si la letra escogida no está en palabra a adivinar, se te resta una vida#
        vida = vida -1
        print("Has fallado, tienes:" + str(vida)+ "vidas")

    if vida ==0: # si tus vidas llegan a 0 , has finalzado
        print("Perdiste,vuelve a intentarlo otra vez")


