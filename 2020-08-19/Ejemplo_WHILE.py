# El bucle while (mientras) ejecuta un fragmento de codigo mientras se
# cumpla una condicion.

salir = False
while not salir:
    entrada = raw_input()
    if entrada == "adios":
        salir = True
    else:
        print(entrada)