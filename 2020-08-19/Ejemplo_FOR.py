# Leamos la cabecera del bucle como si de lenguaje natural se tratara:
# para cada elemento en secuencia. Y esto es exactamente lo que hace
# el bucle: para cada elemento que tengamos en la secuencia, ejecuta
# estas lineas de codigo.
# Lo que hace la cabecera del bucle es obtener el siguiente elemento de
# la secuencia secuencia y almacenarlo en una variable de nombre elemento. 
# Por esta razon en la primera iteracion del bucle elemento valdra
# uno, en la segunda dos, y en la tercera tres.

secuencia = ["uno", "dos", "tres"]

# Recorrido
for elemento in secuencia:
    print(elemento)

# Recorrido Inverso
for elemento in reversed(secuencia):
    print(elemento)

# Avance
print("Numeros del 0 al 5")
for i in range(0,6,1): #range(limite1,limite2,step)
    print(i)

# Retroceso
print("Numeros del 5 al 0")
for i in range(6,0,-1):
    print(i)
