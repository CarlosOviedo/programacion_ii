import random

# JUEGO DEL AHORCADO
# Criterios de Aceptación
# 1) Que se elija al azar una palabra del listado y se muestren 
# los respectivos guiones.
# 2) Que el jugador ingrese una letra y se muestre 
# en el lugar de los guiones o que baje la vida.
# 3) Tenga 3 vidas el jugador.
# 4) Un diccionario de palabras (Lista)
# palabras=["perro","manzana","reja","luz","negro","cortina","juego"]
# 5) Cuando las vidas sean = 0 mostrar "Game Over".
# 6) Cuando se adivine la palabra mostrar "You Win".
# 7) No se puede ingresar dos veces la misma letra

# ANALISIS
# Conocer la palabra.
# Conocer las letras de la palabra.
# Dibujar los guiones.
# While para permitir ingresar las letras
# Condicionales para comparar las letras
# Definir un Array/String Vacío donde guarden las letras adivinadas

#EJEMPLOS
random.randint(0,5) #random.randint(inicio,fin)

array=["uno","dos","tres"]
longitudArray=len(array) #Longitud del Array

string="Hola" # Array de Caracteres -> []
longitudString=len(string) #Longitud del String

print(longitudArray)
print(longitudString)

print(string[0])
print(string[1])
print(string[2])
print(string[3])

