import random

#Definicion de la Funcion
 
def mi_funcion(param1, param2):
    print("Parametro 1 -> " + str(param1))
    print("Parametro 2 -> " + str(param2))

#Llamada a la Funcion
mi_funcion(45,23)

def suma(var1,var2):
    return var1+var2

def mifuncion2(entrada1):
    print("RetornandoValores")
    if entrada1>0:
        if entrada1<0:
            print(1)

sumaTotal=suma(1,2)

print("La suma es "+str(sumaTotal))

print("La suma es "+str(suma(1,2)))

# Pasar el Retorno de una Funcion como parametro a otra
mi_funcion(suma(2,3),10)