# Primero se evalua la condicion del if. Si es cierta, se ejecuta su codigo y 
# se continua ejecutando el codigo posterior al condicional; si no se cumple,
# se evalua la condicion del elif. Si se cumple la condicion del elif
# se ejecuta su codigo y se continua ejecutando el codigo posterior al
# condicional; si no se cumple y hay mas de un elif se continua con el
# siguiente en orden de aparicion. Si no se cumple la condicion del if ni
# de ninguno de los elif, se ejecuta el codigo del else.

numero=0

if numero < 0:
    print("Negativo")
elif numero > 0:
    print ("Positivo")
else:
    print ("Cero")