from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic


class Mensaje:
    def __init__(self,titulo,msj):
        self.msg = QMessageBox()
        self.msg.setWindowTitle(titulo)
        self.msg.setText(msj)
        self.msg.setIcon(QMessageBox.Warning)
        self.msg.setStandardButtons(QMessageBox.Ok )
        
    def mostrar (self):
        self.msg.exec_()
            
