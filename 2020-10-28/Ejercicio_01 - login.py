from PyQt5.QtWidgets import QMainWindow, QApplication, QLineEdit
from PyQt5 import uic
from Ejercicio_01_menu import menu
from Ejercicio_01_message_box import Mensaje

class login(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-28/login.ui", self)
        self.menu = menu()
        self.entrar.clicked.connect(self.login)
        
        self.clave.setEchoMode(QLineEdit.Password)
        self.cerrar.clicked.connect(self.cerrarWin)
        self.mensaje_0 = Mensaje("Error", "Ingrese un usuario y clave")
        self.mensaje_1 = Mensaje("Error", "Usuario Invalido")
        self.mensaje_2 = Mensaje("Error", "Clave Invalida")
        self.mensaje_3 = Mensaje("Error", "Clave Invalida y Usuario Invalido")
        

    def login(self):
        usuario = self.usuario.text()
        clave   = self.clave.text()
        if usuario      ==  "" and clave=="":
            self.mensaje_0.mostrar()       
        elif usuario    ==  "admin" and clave=="admin":
            self.menu.show()
            self.setEnabled(False)
        elif usuario    !=  "admin" and clave=="admin":
            self.mensaje_1.mostrar()           
        elif usuario    ==  "admin" and clave!="admin":
            self.mensaje_2.mostrar()
        elif usuario    !=  "admin" and clave!="admin":
            self.mensaje_3.mostrar()
                

    def cerrarWin(self):
        self.menu.hide()


app = QApplication([])

loginWin = login()
loginWin.show()

app.exec_()