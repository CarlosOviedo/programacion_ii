from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("ejericio_01.ui",self)               #Ubicamos la ventana realizado con "designed"
        
        self.boton.clicked.connect  (self.on_clicked)

    def on_clicked (self):
        
        palabra= self.ingreso.text()
        self.texto.setText(palabra)
        print ("Se hizo click")

app = QApplication([])
win = MiVentana()
win.show()
app.exec_()  